using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerCarController : MonoBehaviour {
	
	[SerializeField] private float torque = 500.0f;
	[SerializeField] private float engineRatio = 4.0f;
	
	private float RPM = 0.0f;
	[SerializeField] private float maxRPM = 2500.0f;
	
	private WheelCollider FLWheel = null;
	private WheelCollider FRWheel = null;
	
	[SerializeField] private float waypointClearDistance = 20.0f;
	[SerializeField] private GameObject waypointInput = null;
	private List<Waypoint> waypointList;
	private int waypointIndex;
	
	public int waypointsCleared {private set; get; }
	public int lap { private set; get; }
	public bool win { private set; get; }
	public float distanceTravelled { private set; get; }
	
	[SerializeField] AudioClip collisionSound = null;
	[SerializeField] AudioClip crashSound = null;
	
	private Game game = null;
	
	// Use this for initialization
	private void Start () {
		this.CarInit();
		this.WaypointsInit();
		this.game = FindObjectOfType(typeof(Game)) as Game;
	}
	
	private void CarInit()
	{
		//search all of car transform's children
		foreach(Transform child in this.transform) {
			
			//find and assign car's center of mass
			if(child.tag == TagMap.CAR_COM) {
				this.rigidbody.centerOfMass = child.localPosition;
				//Debug.Log("Center of mass found");
			}
			
			//find and assign car's front wheels
			if(child.tag == TagMap.CAR_WHEELS) {
				foreach(Transform grandchild in child) {
					if(grandchild.tag == TagMap.CAR_WHEEL_FL) {
						this.FLWheel = grandchild.gameObject.GetComponent<WheelCollider>();
						//Debug.Log(this.FLWheel.name + "found");
					}
					
					if(grandchild.tag == TagMap.CAR_WHEEL_FR) {
						this.FRWheel = grandchild.gameObject.GetComponent<WheelCollider>();
						//Debug.Log(this.FRWheel.name + "found");
					}
				}
			}
		}
		
		this.lap = 0;
		this.waypointsCleared = 0;
		this.distanceTravelled = 0.0f;
	}
	
	private void WaypointsInit()
	{
		this.waypointList = new List<Waypoint>();
		
		foreach(Transform child in this.waypointInput.transform) {
			if(child.GetComponent<Waypoint>())
				this.waypointList.Add(child.GetComponent<Waypoint>());
		}
		
		this.waypointList.Sort(
			delegate(Waypoint w1, Waypoint w2) { return w1.ID.CompareTo(w2.ID); }
		);
		
		this.waypointIndex = 0;
	}
	
	// Update is called once per frame
	private void Update () {
		
		if(!this.game.raceStart) {
			this.FLWheel.motorTorque = this.torque / this.engineRatio * 0.1f;
			this.FRWheel.motorTorque = this.torque / this.engineRatio * 0.1f;
			return;
		}
		
		if(this.lap > 0)
			this.win = true;
		
		//use drag to limit car's velocity
		this.rigidbody.drag = this.rigidbody.velocity.magnitude / 350.0f;
		
		//calculate RPM based on average RPM of two front wheels X engine ratio
		this.RPM = ((this.FLWheel.rpm + this.FRWheel.rpm)/2) * this.engineRatio;
		
		//adjust audio pitch based on RPM
		this.audio.pitch = Mathf.Abs(this.RPM / (this.maxRPM + 10000.0f)) + 0.5f;
		
		if(this.audio.pitch > 1.5f)
			this.audio.pitch = 1.5f;
		
		//apply steering and torque to wheels based on keyboard input
		this.FLWheel.motorTorque = this.torque / this.engineRatio * Input.GetAxis(InputMap.UP_DOWN);
		this.FRWheel.motorTorque = this.torque / this.engineRatio * Input.GetAxis(InputMap.UP_DOWN);
		this.FLWheel.steerAngle = 10.0f * Input.GetAxis(InputMap.LEFT_RIGHT);
		this.FRWheel.steerAngle = 10.0f * Input.GetAxis(InputMap.LEFT_RIGHT);
		
		this.UpdateWaypoints();
		
		this.distanceTravelled += this.rigidbody.velocity.magnitude*1.015f;
	}
	
	private void OnCollisionEnter(Collision col)
	{
		if(col.relativeVelocity.magnitude > 2) {
			
			if(col.gameObject.tag == TagMap.WALL) {
				this.audio.PlayOneShot(this.collisionSound);
			}
			
			if(col.gameObject.tag == TagMap.CAR)
				this.audio.PlayOneShot(this.crashSound);
		}
	}
	/*
	public bool AllWaypointsPassed()
	{
		foreach(Waypoint waypoint in this.waypointList) {
			if(!waypoint.Passed)
				return false;
		}
		
		return true;
	}
	*/
	private void UpdateWaypoints()
	{
		//vector to next waypoint on same y position as car
		Vector3 waypointPos = new Vector3(
			this.waypointList[this.waypointIndex].gameObject.transform.position.x,
			this.transform.position.y,
			this.waypointList[this.waypointIndex].gameObject.transform.position.z
		);
		
		//vector relative to current position of car
		Vector3 relativeWaypointPos = this.transform.InverseTransformPoint(waypointPos);
		
		if(relativeWaypointPos.magnitude < this.waypointClearDistance) {
			this.waypointList[waypointIndex].Passed = true;
			this.waypointIndex++;
			this.waypointsCleared++;
			
			if(this.waypointIndex >= this.waypointList.Count) {
				foreach(Waypoint waypoint in this.waypointList)
					waypoint.Passed = false;
				
				this.waypointIndex = 0;
				this.lap++;
			}
		}
	}
}
