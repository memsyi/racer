using UnityEngine;
using System.Collections;

public static class InputMap {
	public const string UP_DOWN = "Vertical";
	public const string LEFT_RIGHT = "Horizontal";
}
