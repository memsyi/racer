using UnityEngine;
using System.Collections;

public class StartMenu : MonoBehaviour {
	private GameObject pressAnyKey = null;
	
	void Start()
	{
		foreach(Transform child in this.transform)
		{
			if(child.tag == TagMap.PRESS_ANY_KEY)
				this.pressAnyKey = child.gameObject;
		}
	}
	
	void Update()
	{
		this.pressAnyKey.animation.Play();
		
		if(Input.anyKeyDown) {
			Debug.Log("keypress");
			Application.LoadLevel("Game");
		}
	}
}
