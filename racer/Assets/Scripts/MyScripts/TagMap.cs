using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class TagMap {
	public const string CAR = "Car";
	public const string CAR_WHEELS = "CarWheels";
	public const string CAR_COM = "CarCenterOfMass";
	public const string CAR_WHEEL_FL = "CarWheelFL";
	public const string CAR_WHEEL_FR = "CarWheelFR";
	public const string CAR_WHEEL_BL = "CarWheelBL";
	public const string CAR_WHEEL_BR = "CarWheelBR";
	public const string WALL = "Wall";
	public const string PRESS_ANY_KEY = "PressAnyKey";
	public const string HUD_POS = "HUDPOS";
	public const string HUD_LAP = "HUDLAP";
	public const string HUD_SPD = "HUDSPD";
	public const string HUD_WIN = "HUDWIN";
	public const string HUD_QUIT = "HUDQUIT";
	public const string HUD_COUNT = "HUDCOUNT";
	public const string PLAYER = "PlayerCar";
}
