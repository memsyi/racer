using UnityEngine;
using System.Collections;

public class Waypoint : MonoBehaviour {
	public bool CautionArea = false;
	public bool Passed;
	public int ID;

	// Use this for initialization
	void Awake () {
		this.ID = int.Parse(this.gameObject.name);
	}
	
	void Start() {
		this.Passed = false;
	}
}
