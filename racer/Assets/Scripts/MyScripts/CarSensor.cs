using UnityEngine;
using System.Collections;

public class CarSensor: MonoBehaviour {
	private AICarController AICar = null;
	
	// Use this for initialization
	private void Start () {
		this.AICar = this.transform.parent.gameObject.GetComponent<AICarController>();
	}
	
	private void OnTriggerEnter(Collider col) {
		if(col.tag == TagMap.WALL)
			this.AICar.wallNear = true;
	}
	
	private void OnTriggerExit(Collider col) {
		if(col.tag == TagMap.WALL)
			this.AICar.wallNear = false;
	}
}
