using UnityEngine;
using System.Collections;

public class HUD : MonoBehaviour {
	private TextMesh countText = null;
	private TextMesh lapText = null;
	private TextMesh speedText = null;
	private TextMesh positionText = null;
	private TextMesh winText = null;
	private TextMesh quitText = null;
	private PlayerCarController player = null;
	private Game game = null;
	
	private bool raceStart = false;
	private bool win = false;
	private AICarController[] AICars;
	
	private float countDown;
	
	// Use this for initialization
	private void Start ()
	{
		this.HUDInit();
		
		if(this.transform.parent.GetComponent<PlayerCarController>())
			this.player = this.transform.parent.GetComponent<PlayerCarController>();
		
		this.AICars = FindObjectsOfType(typeof(AICarController)) as AICarController[];
		this.game = FindObjectOfType(typeof(Game)) as Game;
		
		this.countDown = Time.time + 4;
	}
	
	private void HUDInit()
	{
		foreach(Transform child in this.transform) {
			if(child.gameObject.GetComponent<TextMesh>()) {
				if(child.tag == TagMap.HUD_POS)
					this.positionText = child.gameObject.GetComponent<TextMesh>();
				
				if(child.tag == TagMap.HUD_LAP)
					this.lapText = child.gameObject.GetComponent<TextMesh>();
				
				if(child.tag == TagMap.HUD_SPD)
					this.speedText = child.gameObject.GetComponent<TextMesh>();
				
				if(child.tag == TagMap.HUD_WIN)
					this.winText = child.gameObject.GetComponent<TextMesh>();
				
				if(child.tag == TagMap.HUD_QUIT)
					this.quitText = child.gameObject.GetComponent<TextMesh>();
				
				if(child.tag == TagMap.HUD_COUNT)
					this.countText = child.gameObject.GetComponent<TextMesh>();
			}
		}
		
		this.positionText.text = "";
		this.lapText.text = "";
		this.speedText.text = "";
		this.winText.text = "";
		this.quitText.text = "";
	}
	
	// Update is called once per frame
	private void Update ()
	{
		if(this.game.raceStart) {
			
			if(this.player.win)
				StartCoroutine("UpdateHUDWin");
			
			this.UpdateHUDSpeed();
			this.UpdateHUDPos();
			this.UpdateHUDLap();
			
		} else {
			//StartCoroutine("StartSequence");
			//StartCoroutine("HUDCountDown");
			this.HUDCountDown();
		}
	}
	
	private void HUDCountDown()
	{
		
		int count = (int)(countDown - Time.time);
		
		if(count < 1) {
			count = 0;
			this.countText.text = count.ToString();
			this.countText.animation.Play("Fade");
			this.game.raceStart = true;
		} else {
			this.countText.text = count.ToString();
		}
	}
	/*
	private IEnumerator StartSequence()
	{
		yield return new WaitForSeconds(2.0f);
		//this.countText.animation.Play("Fade");
		this.countText.text = "3";
		yield return new WaitForSeconds(1.0f);
		//this.countText.animation.Play("Fade");
		this.countText.text = "2";
		yield return new WaitForSeconds(1.0f);
		//this.countText.animation.Play("Fade");
		this.countText.text = "1";
		this.countText.animation.Play("Fade");
		this.countText.text = "0";
		this.raceStart = true;
	}
	*/
	private void UpdateHUDSpeed()
	{
		float speedF = Mathf.Abs(this.player.rigidbody.velocity.magnitude)*3.6f;
		int speedI = (int)speedF;
		this.speedText.text = "SPD: " + speedI.ToString() +"km/hr";
	}
	
	private void UpdateHUDPos()
	{
		this.positionText.text = "POS: " + (4-CarsBehind()) + "/4";
	}
	
	private void UpdateHUDLap()
	{
		this.lapText.text = "LAP: " + this.player.lap +"/1";
	}
	
	private IEnumerator UpdateHUDWin()
	{
		this.winText.text = "WINNER!";
		
		if(!win) {
			this.winText.animation.Play("Winner");
			win = true;
		}
		
		yield return new WaitForSeconds(1.0f);
		
		this.quitText.text = "PRESS ESC TO QUIT";
		this.quitText.animation.Play("Quit");
	}
	
	private int CarsBehind()
	{
		int carsBehind = 0;
		
		foreach(AICarController AICar in this.AICars) {
			if(AICar.distanceTravelled < this.player.distanceTravelled)
				carsBehind++;
		}
		
		return carsBehind;
	}
}
